import numpy as np
from typing import List, Tuple

class MLData(object):
    """
    Class to hold the simulation data
    """

    def __init__(self, xd: np.array, xs: np.array, y: np.array) -> None:
        """
        Intiliatising the formind data

        Args:
            xd (np.array): Dynamic data from simulation (input_num_sample, 365, num_dyn_var)
            xs (np.array): Static data from simulation (input_num_sample, 50, num_stat_var)
            y (np.array): Target year (input_num_sample,)
        """
        self._y = y
        self._xd = xd
        self._xs = xs
        self._ndata = xd.shape[0]
        self._xdvar = xd.shape[2]
        self._xsvar = xs.shape[2]
        self.xdshape = xd.shape

    def get_XY(
        self, n_years: int = 3, classify: bool = False, percentile: int = 90
    ) -> Tuple:
        """Creates Xd (daily-dynamic output), Xs (yearly-static output), and Y (yearly-target)
        Xd shape : (out_num_sample, num_days, num_dyn_var)
        Xs shape : (out_num_sample, num_bins, num_stat_var)
        Y shape : (out_num_sample, )

        Args:
            n_days (int, optional): num of days in the data. Defaults to 1000.
            classify (bool, optional): Turn target Y to boolean based on percentile. Defaults to False.
            percentile (int, optional): Percentile value for classification of target Y . Defaults to 90.

        Returns:
            Tuple: Xd, Xs, Y
        """

        Xd, Xs, Y = self._reorder_year(
            self._xd, self._xs, self._y, n_year=n_years
        )

        if classify == True:
            Y = self._classify(Y, percentile=percentile)

        return Xd[:, :, :, np.newaxis], Xs, Y

    def _reorder_year(
        self, Xd: np.array, Xs: np.array, Y: np.array, n_year: int
    ) -> Tuple:
        """Reordering input. For example stacking multiple years/months as 1 sample dynamic input.
        Static input of only year previous to climate data is taken. The output is taken for the last year input of the climate.

        Args:
            Xd (np.array): dynamic input
            Xs (np.array): static input
            Y (np.array): target input
            n_year (int): number of years in input sample

        Returns:
            Tuple: Xd_out, Xs_out, Y_out
        """

        timestep = Xd.shape[1]
        n_data_new = self._ndata - n_year
        Xd_out = np.zeros((n_data_new, timestep * n_year, self._xdvar))

        for i in range(n_data_new):
            for j in range(self._xdvar):
                Xd_out[i, :, j] = Xd[i + 1 : i + 1 + n_year, :, j].flatten()

        Xs_out = Xs[:-n_year]

        Y_out = Y[n_year:]

        return Xd_out, Xs_out, Y_out

    def _classify(self, Y: np.array, percentile: int) -> np.array:
        """Classify values in a numpy array based on the percentile value
        The values greater than the percentile as converted 1 and others as 0

        Args:
            Y (np.array): Input array to be converted in binary classes
            percentile (int): threshold percentile for binarization

        Returns:
            np.array: Binarized output array
        """

        perc = np.percentile(Y, percentile)

        Y[Y < perc] = 0
        Y[Y > 0] = 1

        return Y

    def _year_ceil(self, n_days: int) -> int:
        """Finds the number of years required to get the number of days

        Args:
            n_days (int): number of days

        Returns:
            int: ceiling year from number of days
        """
        if n_days % 365 == 0:
            n_year = n_days // 365
        else:
            n_year = n_days // 365 + 1

        return n_year
