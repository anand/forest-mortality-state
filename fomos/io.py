#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ==========================================================
# Created by : Mohit Anand
# Created on : Mon Apr 17 2023 at 12:23:53 PM
# ==========================================================
# Created on Mon Apr 17 2023
# __copyright__ = Copyright (c) 2023, Mohit Anand's Project
# __credits__ = [Mohit Anand,]
# __license__ = Private
# __version__ = 0.0.0
# __maintainer__ = Mohit Anand
# __email__ = itsmohitanand@gmail.com
# __status__ = Development
# ==========================================================

import numpy as np
import h5py
from typing import Tuple, List


class FormindData():
    def __init__(
            self, 
            data_path="/data/compoundx/anand/forest-mortality/formind_sim/", 
            agg = "monthly",
            pft="beech", 
            xs_list =["age_dist", "sv_dist", "laitree_dist", "h_dist", "d_dist"],
            y = "MBR") -> None:
        
        self.agg = agg
        self.sim_dir = data_path +f"{agg}/"
        self.pft = pft
        self.y = y
        self.xs_list = xs_list
        num_files = 20

        self.train_index, self.val_index, self.test_index = self._create_index(
            num_sample=num_files * 7999
        )
        
    def _read_formind_sim(self, index: np.array) -> Tuple:
        """Main function to read the FORMIND data

        Args:
            index (np.array): The indexes that should be read

        Returns:
            Tuple: Tuple of numpy arrays with Xd, Xs, bin and Y
        """

        index = sorted(index)

        pft_path = self.sim_dir + f"{self.pft}_dynMort_{self.agg}_10000ha.h5"
        dist_list = [element for element in self.xs_list if "dist" in element]

        if self.agg == "monthly":
            agg_n = 12
        elif self.agg == "pentad":
            agg_n = 365//5
        elif self.agg == "daily":
            agg_n = 365
        elif self.agg == "quarterly":
            agg_n = 4
        else:
            raise KeyError
        

        Xd = np.zeros((len(index), agg_n, 3))
        Xs = np.zeros((len(index), 101, len(dist_list)))

        bin = dict()
        with h5py.File(pft_path, "r") as f:

            Xd[:, :, 0] = f["irr"][index]
            Xd[:, :, 1] = f["rain"][index]
            Xd[:, :, 2] = f["temp"][index]

            for i, xs_name in enumerate(dist_list):
                Xs[:, :, i] = f[xs_name][index]
                bin_name = xs_name.replace("dist", "bin")
                bin[bin_name + f"_{i}"] = f[bin_name][:]

            Xd = Xd[1:]
            Xs = Xs[1:]

            if self.y == "ALL":
                Y1 = f["MBR"][index][1:]
                Y2 = f["GPP"][index][:-1] 
                Y3 = f["NEE"][index][:-1] 
                Y4 = f["MNR"][index][1:] 

                Y = np.stack([Y1, Y2, Y3, Y4]).transpose()
                
            else:
                Y = f[self.y][index]

                if (self.y == "MBR") or (self.y == "MNR") :
                    Y = Y[1:]
                    
                elif (self.y == "NEE") or (self.y == "GPP"):
                    Y = Y[:-1]
            
        return Xd, Xs, bin, Y
    
    def _create_index(
        self, num_sample: int, split: List = [0.8, 0.1]
    ) -> Tuple:
        """Generate training, validation and test index based on split

        Args:
            num_sample (int): Total num of samples
            split (List, optional): Training and validation split. Defaults to [0.8, 0.1].

        Returns:
            Tuple: (train_index, val_index, test_index)
        """
        assert sum(split) < 1, "Split fraction sum should be less than 1"
        assert (
            len(split) == 2
        ), "Only provide train and val fraction, test fraction is computed as 1-(train+val) fraction"

        index = np.arange(num_sample)

        train_index = index[: int(split[0] * num_sample)]
        val_index = index[
            int(split[0] * num_sample) : int(sum(split) * num_sample)
        ]
        test_index = index[int(sum(split) * num_sample) :]

        return train_index, val_index, test_index
    
    def get_train(self, index=None) -> Tuple:
        """Get training data

        Args:
            index (None): Index to be included in the data, by default random index will be used.

        Returns:
            Tuple: Returns a Tuple of Xd, Xs, bin and Y
        """
        if index == None:
            index = self.train_index
        return self._read_formind_sim(index)

    def get_val(self, index=None) -> Tuple:
        """Get validation data

        Args:
            index (None): Index to be included in the data, by default random index will be used.

        Returns:
            Tuple: Returns a Tuple of Xd, Xs, bin and Y
        """
        if index == None:
            index = self.val_index
        return self._read_formind_sim(index)

    def get_test(self, index=None) -> Tuple:
        """Get testing data

        Args:
            index (None): Index to be included in the data, by default random index will be used.

        Returns:
            Tuple: Returns a Tuple of Xd, Xs, bin and Y
        """
        if index == None:
            index = self.test_index
        return self._read_formind_sim(index)