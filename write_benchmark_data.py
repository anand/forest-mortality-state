from fomos.io import FormindData
from fomos.data import MLData
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import PrecisionRecallDisplay
from sklearn.metrics import f1_score
import copy
import numpy as np
import h5py

agg="pentad"
n_years = 3
pft="beech"

def write_benchmark_data(filename, var_list, arr_list):
    with h5py.File(filename, "w") as f:
        for i, each in enumerate(var_list):
            if each == "bin":
                for key, val in arr_list[i].items():
                    f[key] = val
            else:    
                f[each] = arr_list[i]

directory = "/data/compoundx/anand/benchmark-dataset/" 
filename = directory + f"train_ALL_{pft}_{agg}_{n_years}_years_10000ha.h5"
train_data = FormindData(agg=agg, pft=pft, y="ALL").get_train()
Xd, Xs, bin, Y = train_data
data = MLData(Xd ,Xs, Y)
Xd, Xs, Y = data.get_XY(classify=False, n_years=n_years)
write_benchmark_data(filename, ["Xd", "Xs", "bin", "Y"], [Xd, Xs, bin, Y])

filename = directory + f"val_ALL_{pft}_{agg}_{n_years}_years_10000ha.h5"
train_data = FormindData(agg=agg, pft=pft, y="ALL").get_val()
Xd, Xs, bin, Y = train_data
data = MLData(Xd ,Xs, Y)
Xd, Xs, Y = data.get_XY(classify=False, n_years=n_years)
write_benchmark_data(filename, ["Xd", "Xs", "bin", "Y"], [Xd, Xs, bin, Y])

filename = directory + f"test_ALL_{pft}_{agg}_{n_years}_years_10000ha.h5"
train_data = FormindData(agg=agg, pft=pft, y="ALL").get_test()
Xd, Xs, bin, Y = train_data
data = MLData(Xd ,Xs, Y)
Xd, Xs, Y = data.get_XY(classify=False, n_years=n_years)
write_benchmark_data(filename, ["Xd", "Xs", "bin", "Y"], [Xd, Xs, bin, Y])
